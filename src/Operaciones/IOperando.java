package Operaciones;

public interface IOperando<Valor> {
	public Valor getValor();
	public void setValor(long v);
	public void setValor(Valor v);
	public void sumar(IOperando<Valor> otroOperando);
	public void restar(IOperando<Valor> otroOperando);
	public void multiplicar(IOperando<Valor> otroOperando);
	public void dividirEntero(IOperando<Valor> otroOperando);
	public void modulo(IOperando<Valor> otroOperando);
	public void elevarYmodulo(IOperando<Valor> exponente, IOperando<Valor> modulo);
	public void uniforme(IOperando<Valor> a, IOperando<Valor> b);
	public IOperando<Valor> clone();
}
