package Primalidad;

import Operaciones.IOperando;

public interface IDeteccionPrimalidad<Valor> {
	public boolean comprobar(IOperando<Valor> candidato);
}
