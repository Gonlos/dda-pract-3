package Operaciones;

import java.util.Random;

public class OperandoLong implements IOperando<Long> {
	private long valorOperando;
	
	public OperandoLong(long valorOperando) {
		super();
		this.valorOperando = valorOperando;
	}
	
	public OperandoLong() {
		super();
		this.valorOperando = 0;
	}
	
	@Override
	public Long getValor() {
		// TODO Auto-generated method stub
		return this.valorOperando;
	}

	@Override
	public void sumar(IOperando<Long> otroOperando) {
		// TODO Auto-generated method stub
		this.valorOperando += otroOperando.getValor();
	}

	@Override
	public void restar(IOperando<Long> otroOperando) {
		// TODO Auto-generated method stub
		this.valorOperando -= otroOperando.getValor();
	}

	@Override
	public void multiplicar(IOperando<Long> otroOperando) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dividirEntero(IOperando<Long> otroOperando) {
		// TODO Auto-generated method stub
		this.valorOperando /= otroOperando.getValor();
	}
	
	@Override
	public void elevarYmodulo(IOperando<Long> exponente, IOperando<Long> modulo) {
		// TODO Auto-generated method stub
		long i = exponente.getValor();
		long r = 1;
		long x = this.valorOperando;
		long z = modulo.getValor();
		while ( i > 1 ) {
			// si i es impar
			if ( (i & 1) == 1 ) {
				r = (r*x) % z;
			}
			
			x = (x*x) % z;
			// dividimos i entre 2
			i >>= 1;
		}
		
		if ( i == 1 ) {
			this.valorOperando = (r*x) % z;
		} else {
			this.valorOperando = r;
		}
	}

	@Override
	public void setValor(long v) {
		// TODO Auto-generated method stub
		this.valorOperando = v;
	}

	@Override
	public void setValor(Long v) {
		// TODO Auto-generated method stub
		this.valorOperando = v;
	}

	@Override
	public IOperando<Long> clone() {
		// TODO Auto-generated method stub
		return new OperandoLong( this.valorOperando );
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OperandoLong other = (OperandoLong) obj;
		if (valorOperando != other.valorOperando)
			return false;
		return true;
	}

	@Override
	public void modulo(IOperando<Long> otroOperando) {
		// TODO Auto-generated method stub
		this.valorOperando %= otroOperando.getValor();
	}

	@Override
	public void uniforme(IOperando<Long> a, IOperando<Long> b) {
		// TODO Auto-generated method stub
	    Random rand = new Random();

	    // FIXME java no maneja muy bien los numeros aleatorios
	    // de tipo long
	    this.valorOperando = Math.abs( rand.nextLong() );
	    this.valorOperando %= (b.getValor() - a.getValor() + 1);
	    this.valorOperando += a.getValor();
	}
}
