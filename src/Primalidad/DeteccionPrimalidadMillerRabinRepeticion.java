package Primalidad;

import Operaciones.IOperando;

public class DeteccionPrimalidadMillerRabinRepeticion<Valor> implements IDeteccionPrimalidad<Valor> {
	private IOperando<Valor> valor0;
	private IOperando<Valor> valor1;
	private IOperando<Valor> valor2;
	private IOperando<Valor> valor3;
	private int numRepeticiones;
	
	public DeteccionPrimalidadMillerRabinRepeticion( int numRepeticiones ) {
		super();
		this.valor0 = null;
		this.valor1 = null;
		this.valor2 = null;
		this.numRepeticiones = numRepeticiones;
	}
	
	private void rellenarValores( IOperando<Valor> numero ) {
		if ( this.valor0 == null ) {
			this.valor0 = numero.clone();
			this.valor0.setValor(0);
		}

		if ( this.valor1 == null ) {
			this.valor1 = numero.clone();
			this.valor1.setValor(1);
		}
		
		if ( this.valor2 == null ) {
			this.valor2 = numero.clone();
			this.valor2.setValor(2);
		}
		
		if ( this.valor3 == null ) {
			this.valor3 = numero.clone();
			this.valor3.setValor(3);
		}
	}
	
	@Override
	public boolean comprobar(IOperando<Valor> candidato) {
		// TODO Auto-generated method stub		
		rellenarValores(candidato);
		
		IOperando<Valor> nTmp = candidato.clone();
		
		// dado que el valor de candidato tiene que ser un numero
		// impar mayor que cuatro comprobamos antes dichas propiedades
		
		// si el valor es 1,2 o 3 entonces candidato es primo
		if ( candidato.equals( valor1 ) || 
			 candidato.equals( valor2 ) ||
			 candidato.equals( valor3 ) ) {
			return true;
		}
				
		// en el caso contrario cualquier numero par no es primo
		nTmp.modulo( valor2 );
		if ( nTmp.equals( valor0 ) ) {
			return false;
		}
		
		for ( int i = this.numRepeticiones; i > 0; i-- ) {
			if ( !millerRabin(candidato) ) {
				return false;
			}
		}
		
		return true;
	}
	
	private boolean millerRabin( IOperando<Valor> candidato ) {
		IOperando<Valor> nTmp = candidato.clone();
		IOperando<Valor> a = candidato.clone();
		
		nTmp.restar( valor2 );
		a.uniforme( valor2, nTmp );
		
		return perteneceB( a, candidato );
	}
	
	private boolean perteneceB(IOperando<Valor> candidato, IOperando<Valor> numero) {
		IOperando<Valor> s = this.valor0.clone();
		IOperando<Valor> t = numero.clone();
		IOperando<Valor> tTmp;
		IOperando<Valor> numeroMenos1;
		IOperando<Valor> x = candidato.clone();
		
		t.restar( this.valor1 );
		numeroMenos1 = t.clone();
		
		do {
			s.sumar( this.valor1 );
			t.dividirEntero( this.valor2 );
			tTmp = t.clone();
			tTmp.modulo( this.valor2 );
		} while ( !tTmp.equals( this.valor1 ) );
		
		x.elevarYmodulo( t, numero );
		
		if ( x.equals( this.valor1 ) || x.equals( numeroMenos1 ) ) {
			return true;
		}
		
		s.restar( this.valor1 );
		while ( !s.equals( this.valor0 ) ) {
			x.elevarYmodulo( this.valor2, numero);
			
			if ( x.equals( numeroMenos1 ) ) {
				return true;
			}
			
			s.restar( this.valor1 );
		}
		
		return false;
	}

}
