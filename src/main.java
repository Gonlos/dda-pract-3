import java.util.Scanner;

import javax.print.attribute.standard.Compression;

import Operaciones.OperandoLong;
import Primalidad.DeteccionPrimalidadMillerRabinRepeticion;
import Primalidad.IDeteccionPrimalidad;

public class main {
	public static void main(final String[] args) {
		Scanner scanner = new Scanner( System.in );
		
		long numLong;
		int numRepeticiones;
		boolean esPrimo;
		char opcion;
		boolean salir;
		
		OperandoLong numOperando;
		IDeteccionPrimalidad detector;
		
		System.out.println ("####################################################################");
		System.out.println ("#                Practica 3 de Diseno de Algoritmos                #");
		System.out.println ("# ---------------------------------------------------------------- #");
		System.out.println ("#   Deteccion de la primalidad de un numero mediante un algoritmo  #");
		System.out.println ("#   probabilistico de tipo Las Vegas                               #");
		System.out.println ("####################################################################");
		System.out.println ("");
		
		do {			
			System.out.println ("Seleccione la opcion deseada");
			
			System.out.println ("  1: Comprobar la primalidad de un numero");
			System.out.println ("  2: Calcular un rango de numeros primos mediante el metodo probabilistico Miller-Rabin");
			System.out.println ("  3: Calcular un rango de numeros primos mediante un metodo determinista");
			System.out.println ("  4: Comprobar con que numeros falla el metodo probabilista");
			System.out.println ("");
			System.out.print ("Opcion: ");
			opcion = scanner.next().charAt(0);
			
			switch ( opcion ) {
			case '1':
				comprobarPrimalidad( scanner );
				break;
				
			case '2':
				rangoNumerosPrimosProbabilista( scanner );
				break;
				
			case '3':
				rangoNumerosPrimosDeterminista( scanner );
				break;
				
			case '4':
				falsosNumerosPrimos( scanner );
				break;

			default:
				break;
			}
			
			System.out.println ("Desea continuar? [y,N]");
			opcion = scanner.next().charAt(0);
			
		} while ((opcion == 'Y') || (opcion == 'y'));
	}

	public static void rangoNumerosPrimosDeterminista( Scanner scanner ) {
		long numLongInicio, numLongFinal;
		long numMax;
		boolean esPrimo;
		
		System.out.println("Cual es el numero de inico del rango?");
		numLongInicio = scanner.nextLong();
		
		System.out.println("Cual es el numero final del rango?");
		numLongFinal = scanner.nextLong();
		
		System.out.println("Los siguientes numeros son primos:");
		
		for ( long i = numLongInicio;
				i <= numLongFinal; i++ ) {
			if ( esPrimo( i ) ) {
				System.out.printf("  %d\n", i );
			}
		}
	}
	
	public static boolean esPrimo( long n ) {
		if ( n == 2 ) return true;
		
		long numMax = (long) Math.ceil( Math.sqrt( n ) );
		boolean esPrimo = true;
		
		for( long i = 2; i <= numMax; i++ ) {
			if ( n % i == 0 ) {
				esPrimo = false;
				break;
			}
		}
		
		return esPrimo;
	}

	public static void falsosNumerosPrimos( Scanner scanner ) {
		long numLongInicio, numLongFinal;
		int numRepeticiones;
		boolean prob, deter;
		
		IDeteccionPrimalidad detector;
		
		System.out.println("Cuantas repeticiones desea que realize el algoritmo?");
		numRepeticiones = scanner.nextInt();
		
		System.out.println("Cual es el numero de inico del rango?");
		numLongInicio = scanner.nextLong();
		
		System.out.println("Cual es el numero final del rango?");
		numLongFinal = scanner.nextLong();
		
		System.out.println("El algoritmo probabilista ha fallado al detectar los siguientes numeros primos:");
		
		detector = new DeteccionPrimalidadMillerRabinRepeticion<Long>( numRepeticiones );
		int n = 0;
		for ( long i = numLongInicio; i < numLongFinal; i++ ) {
			prob = detector.comprobar( new OperandoLong( i ) );
			deter = esPrimo( i );
			if ( prob != deter ) {
				System.out.printf("  Fallo %d (Numero %d):\n", n, i );
				System.out.printf("    Algoritmo Probabilista: %s\n", prob ? "Es Primo" : "No es primo" );
				System.out.printf("    Algoritmo Determinista: %s\n", deter ? "Es Primo" : "No es primo" );
				n++;
			}
		}
		
		System.out.printf("Numero de falsas respuestas: %d/%d\n", n, numLongFinal - numLongInicio + 1 );
	}

	public static void rangoNumerosPrimosProbabilista( Scanner scanner ) {
		long numLongInicio, numLongFinal;
		int numRepeticiones;
		
		OperandoLong numOperando;
		IDeteccionPrimalidad detector;
		
		System.out.println("Cuantas repeticiones desea que realize el algoritmo?");
		numRepeticiones = scanner.nextInt();
		
		System.out.println("Cual es el numero de inico del rango?");
		numLongInicio = scanner.nextLong();
		
		System.out.println("Cual es el numero final del rango?");
		numLongFinal = scanner.nextLong();
		
		System.out.println("Se han detectado los siguientes numeros como primos:");
		
		detector = new DeteccionPrimalidadMillerRabinRepeticion<Long>( numRepeticiones );
		for ( long i = numLongInicio; i < numLongFinal; i++ ) {
			numOperando = new OperandoLong( i );
			if ( detector.comprobar( numOperando ) ) {
				System.out.printf("  %d\n", i );
			}
		}
	}
	
	public static void comprobarPrimalidad( Scanner scanner) {
		long numLong;
		int numRepeticiones;
		boolean esPrimo;
		
		OperandoLong numOperando;
		IDeteccionPrimalidad detector;
		
		System.out.println("De que numero desea comprobar la primalidad?");
		numLong = scanner.nextLong();
		numOperando = new OperandoLong( numLong );
		
		System.out.println("Cuantas repeticiones desea que realize el algoritmo?");
		numRepeticiones = scanner.nextInt();
		
		detector = new DeteccionPrimalidadMillerRabinRepeticion<Long>( numRepeticiones );
		esPrimo = detector.comprobar( numOperando );
		
		if ( esPrimo ) {
			System.out.printf("Se ha detectado que %d es primo con %d repeticiones.\n", numLong, numRepeticiones );
		} else {
			System.out.printf("No se ha detectado que %d sea primo con %d repeticiones.\n", numLong, numRepeticiones );
		}
	}
	
	
}
